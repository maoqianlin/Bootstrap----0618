$(function () {

    // 点击在线客服，弹窗出现
    $(".youzhong").click(function () {
        $(".kefu").show();
    })

    // 点击弹窗按钮，弹窗隐藏
    $(".kefu2").click(function () {
        $(".kefu").hide();
    })

    // 点击在线咨询，留言框出现在屏幕中央，左下角弹窗消失
    $(".youxia").click(function () {
        $(".liuyan").show().css('bottom', '35%').css('left', '45%');
        $(".zuoxia").hide();
    })

    // 点击留言框最小化按钮，留言框消失，左下角弹窗出现
    $(".liuyan2").click(function () {
        $(".liuyan").hide();
        $(".zuoxia").show();
    })

    // 点击左下角弹窗放大按钮，弹窗消失，留言框出现
    $(".zuoxia2").click(function () {
        $(".zuoxia").hide();
        $(".liuyan").show();
    })



})